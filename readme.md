# asi-eggrider

This program is a simple tool that allows you to setup [ASI electronic speed controllers](https://www.acceleratedsystems.com/products/electric-motor-controllers/bac355-bac555) for use with the [Eggrider display/controller](https://eggrider.com/), set and read individual registers, and crack some access controls.

## Installation

For these instructions, I assume you have rust already installed with cargo.  If this is not the case, I recommend you checkout [rustup](https://rustup.rs/).

Once you have rust, checkout this repo

`git clone https://gitlab.com/Housedillon/asi-eggrider.git`

cd into its directory and run it

```
cd asi-eggrider
cargo run -- --help
```

This will print the built-in help:

```
Tool to configure ASI ESCs.  This program can be used to set individual ModBus registers, read or dump registers setup an ESC to use Eggrider, or crack user access codes. Options are short, and verbs are long lables

Usage: asi-eggrider [OPTIONS] -p <PORT>

Options:
  -p <PORT>                    Serial port to the ESC
  -b <BAUD>                    Baud rate [default: 115200]
  -d <DEVICE_ID>               ModBus device ID [default: 1]
  -a <ACCESS>                  Access code for parameter access (register 62) [default: 0]
  -f                           Write to flash after an operation
      --eggrider               Perform eggrider configuration
      --dump                   Dump registers
      --read <READ>            Read a single register
      --crack                  Attempt to crack the read access code (register 62)
      --crack-oem              Attempt to crack the OEM User Access Level
      --write <WRITE> <WRITE>  Write a single register (register first, value second)
  -h, --help                   Print help
  -V, --version                Print version
```

# Example usecases

_I want to setup my ESC to use with the Eggrider, and it doesn't have a passcode set_

`cargo run -- --p /dev/ttyUSB0 --eggrider`

_I have a locked ESC, and I need to know what the passcode is_

`cargo run -- --p /dev/ttyUSB0 --crack`

_I'd like to adjust the field weakening settings, but I don't have the oem code_

`cargo run -- --p /dev/ttyUSB0 --crack-oem`

# More information

This tool is part of a blog series that I've written.  It'll make more sense if you read it.

In [part 1](/blog/flash-part-one) of the series, I introduced the background of this particular exercise, and why we're embarking on it.  I disassembled the bike and identified the major pieces of its systems in [part 2](/blog/flash-part-two).  And in [part 3](/blog/flash-part-three) we looked at the mainboard, figured out what its primary components were, and how they worked.  We reverse engineered the BMS, in particular the communication protocol, in [part 4](/blog/flash-part-four).  In [part 5](/blog/flash-part-five), we discovered the make and model of the ESC, and figured out our plan for moving forward with the project.

