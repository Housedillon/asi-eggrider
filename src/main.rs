use clap::Parser;
use color_eyre::{eyre::eyre, Result};
use log::{error, info, trace, warn};
use std::{str, time::Duration};
use tokio_modbus::client::Context;
use tokio_modbus::prelude::*;
use tokio_serial::SerialStream;

/// Tool to configure ASI ESCs.  This program can be used
/// to set individual ModBus registers, read or dump registers
/// setup an ESC to use Eggrider, or crack user access codes.
/// Options labels are short, and verbs are long.
#[derive(Parser, Debug)]
#[command(author, version, about, long_about = None)]
struct Args {
    /// Serial port to the ESC
    #[arg(short)]
    port: String,

    /// Baud rate
    #[arg(short, default_value_t = 115200)]
    baud: u32,

    /// ModBus device ID
    #[arg(short, default_value_t = 1)]
    device_id: u8,

    /// Access code for parameter access (register 62)
    #[arg(short, default_value_t = 0)]
    access: u16,

    /// Write to flash after an operation
    #[arg(short, default_value_t = true)]
    flash: bool,

    /// Perform eggrider configuration
    #[arg(long, default_value_t = false)]
    eggrider: bool,

    /// Dump registers
    #[arg(long, default_value_t = false)]
    dump: bool,

    /// Read a single register
    #[arg(long)]
    read: Option<u16>,

    /// Attempt to crack the read access code (register 62)
    #[arg(long)]
    crack: bool,

    /// Attempt to crack the OEM User Access Level
    #[arg(long)]
    crack_oem: bool,

    /// Write a single register (register first, value second)
    #[arg(long, num_args = 2)]
    write: Option<Vec<u16>>,
}

async fn write_generic(context: &mut Context, address: u16, value: u16) -> Result<()> {
    let result = context
        .write_multiple_registers(address, &vec![value])
        .await;
    if let Err(error) = result {
        warn!("Error writing parameter to ESC at address 66: {}", error);
        // return Err(error);
    }

    Ok(())
}

async fn read_flash_read_access(context: &mut Context) -> Result<u16> {
    let rsp = context.read_holding_registers(62, 0x0001).await?;
    Ok(rsp[0])
}

async fn write_flash_read_access(context: &mut Context, value: u16) -> Result<()> {
    write_generic(context, 62, value).await
}

async fn unlock_paramaters(context: &mut Context, value: u16) -> Result<()> {
    write_generic(context, 498, value).await
}

async fn read_display_protocol(context: &mut Context) -> Result<u16> {
    let rsp = context.read_holding_registers(66, 0x0001).await?;
    Ok(rsp[0])
}

async fn write_display_protocol(context: &mut Context, value: u16) -> Result<()> {
    write_generic(context, 66, value).await
}

// https://support.accelerated-systems.com/KB/?p=664

async fn read_assist_mode(context: &mut Context) -> Result<u16> {
    let rsp = context.read_holding_registers(210, 0x0001).await?;
    Ok(rsp[0])
}

async fn write_assist_mode(context: &mut Context, value: u16) -> Result<()> {
    write_generic(context, 210, value).await
}

async fn save_flash(context: &mut Context) -> Result<()> {
    write_generic(context, 0x01FF, 0x7FFF).await?;

    // https://support.accelerated-systems.com/KB/?p=1188

    // Wait a few seconds for operation to complete.
    // You *can* poll for completion, but tokio-modbus doesn't support that for ... reasons.
    tokio::time::sleep(Duration::from_secs(2)).await;

    let resp = context.read_holding_registers(0x01FF, 1).await?;
    match resp[0] {
        0x1000 => Ok(()),
        0x2000 => Err(eyre!("Failed to save flash memory to ESC.")),
        _ => Err(eyre!(
            "Unexpected and unknown response from ESC: {}",
            resp[0]
        )),
    }
}

async fn do_eggrider(ctx: &mut Context, flash: bool) -> Result<()> {
    // Reading starting values
    let read_access = read_flash_read_access(ctx).await?;
    trace!(
        "Read initial flash parameter read access code (62): {}",
        read_access
    );

    let display_protocol = read_display_protocol(ctx).await?;
    trace!(
        "Read initial display protocol setting (66): {}",
        display_protocol
    );

    let assist_mode = read_assist_mode(ctx).await?;
    trace!(
        "Read initial assist mode source setting (210): {}",
        assist_mode
    );

    info!("Setting values as necessary for EggRider:");

    // Setting desired values
    let desired_ra = 0;
    write_flash_read_access(ctx, desired_ra).await?;
    let new_ra = read_flash_read_access(ctx).await?;
    if new_ra != desired_ra {
        return Err(eyre!(
            "Setting the read access value failed! ({} != {})",
            new_ra,
            desired_ra
        ));
    }
    trace!("Wrote read access to {}", new_ra);

    let desired_dp = 0;
    write_display_protocol(ctx, desired_dp).await?;
    let new_dp = read_display_protocol(ctx).await?;
    if new_dp != desired_dp {
        return Err(eyre!(
            "Setting the display protocol value failed! ({} != {})",
            new_dp,
            desired_dp
        ));
    }
    trace!("Wrote display protocol to {}", new_dp);

    let desired_as = 5;
    write_assist_mode(ctx, desired_as).await?;
    let new_as = read_assist_mode(ctx).await?;
    if new_as != desired_as {
        return Err(eyre!(
            r#"Setting the assist source value failed ({} != {}).
Your ESC is likely locked.
The access code must be set on the command-line if this is the case.
If you don't know the access code, you can try to crack it using this tool."#,
            new_as,
            desired_as
        ));
    }
    trace!("Wrote assist source to {}", new_as);

    info!("Saving values to flash");

    if flash {
        save_flash(ctx).await?;
    }

    Ok(())
}

async fn do_dump(ctx: &mut Context) -> Result<()> {
    for i in 0..530 {
        if let Ok(value) = ctx.read_holding_registers(i, 1).await {
            println!("{}: {:?}", i, value);
        } else {
            println!("{}: error", i);
        }
    }

    Ok(())
}

async fn do_crack(ctx: &mut Context) -> Result<()> {
    println!("Attempting to crack the access code for register 62");

    for i in 1..0xFFFF {
        if i % 0x0100 == 0 {
            println!("{}% complete", (i as f32 / 0xFFFF as f32) * 100 as f32);
        }

        unlock_paramaters(ctx, i).await?;
        match ctx.read_holding_registers(1, 1).await {
            Ok(value) => {
                if value[0] != 0 {
                    println!("Found access code!: {}", i);
                    return Ok(());
                }
            }
            Err(error) => {
                error!("Error occured while cracking: {}", error);
                return Err(eyre!("{}", error));
            }
        }
    }

    Ok(())
}

async fn do_crack_oem(ctx: &mut Context) -> Result<()> {
    println!("Attempting to crack the OEM User Access Code");

    for i in 1..0xFFFF {
        if i % 0x0100 == 0 {
            println!("{}% complete", (i as f32 / 0xFFFF as f32) * 100 as f32);
        }

        write_generic(ctx, 509, i).await?;
        match ctx.read_holding_registers(450, 1).await {
            Ok(value) => {
                if value[0] != 0 {
                    println!("Found access code!: {}", i);
                    return Ok(());
                }
            }
            Err(error) => {
                error!("Error occured while cracking: {}", error);
                return Err(eyre!("{}", error));
            }
        }
    }

    Ok(())
}

async fn do_write(ctx: &mut Context, register: u16, value: u16, flash: bool) -> Result<()> {
    write_generic(ctx, register, value).await?;
    let new_value = ctx.read_holding_registers(register, 1).await?;
    if new_value[0] != value {
        return Err(eyre!(
            "Setting the value for register {} failed! ({} != {})",
            register,
            value,
            new_value[0]
        ));
    }
    trace!("Wrote {} to {}", new_value[0], register);

    if flash {
        save_flash(ctx).await?;
    }

    Ok(())
}

#[tokio::main(flavor = "current_thread")]
pub async fn main() -> Result<()> {
    pretty_env_logger::init();
    color_eyre::install()?;

    trace!("Parsing arguments");
    let args = Args::parse();
    trace!(
        "Preparing to open ESC connection on {} at {} baud.",
        args.port,
        args.baud
    );

    // Setting-up ModBus
    let slave = Slave(args.device_id);

    let builder = tokio_serial::new(args.port, args.baud);
    let port = SerialStream::open(&builder).unwrap();

    let mut ctx = rtu::attach_slave(port, slave);
    info!("Connected to device; reading existing values:");

    if args.access != 0 {
        unlock_paramaters(&mut ctx, args.access).await?;
    }

    if args.eggrider {
        do_eggrider(&mut ctx, args.flash).await?
    }

    if args.dump {
        do_dump(&mut ctx).await?
    }

    if args.crack {
        do_crack(&mut ctx).await?
    }

    if args.crack_oem {
        do_crack_oem(&mut ctx).await?
    }

    if let Some(write) = args.write {
        do_write(&mut ctx, write[0], write[1], args.flash).await?
    }

    if let Some(register) = args.read {
        if let Ok(value) = ctx.read_holding_registers(register, 1).await {
            println!("{}", value[0]);
        }
    }

    Ok(())
}
